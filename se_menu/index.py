import json

def travelMenu(k, d):
    if type(d) != dict:
        # print(k)
        yield (k, d)
        return 

    for n in d.keys():
        for t in travelMenu(k + n, d[n]):
            yield t

def genNGram(s, n):
    for i in range(len(s)-n+1):
        yield s[i:i+n]

def genAllGram(s):
    for n in range(len(s)):
        for ss in genNGram(s, n+1):
            yield ss

if __name__ == '__main__':
    s = """oceanSiuMei = 海大燒臘
usfo = 牛膳坊
uzo = 吮指王
818 = 818
smile_spaghetti = 微笑義大利麵
mzchan = 美芝城
gyen = 金園
3q = 3q雞排
apple_brunch = 蘋果早午餐
beining = 北寧早餐店"""
        # s = "smile_spaghetti = 微笑義大利麵"
    menuIndex = dict()
    itemName2ID = dict()
    ID2ItemName = dict()
    IDCount = 0
    for line in s.split('\n'):
        name, restaurant = line.split(' = ')
        # json_path = './json_test/%s.json' % name
        json_path = './json/%s.json' % name
        menu = json.load(open(json_path, 'r', encoding='UTF-8'))
        menu['name'] = restaurant
        for item, price in travelMenu(str(), menu['root']):
            itemName2ID[restaurant+item] = IDCount
            ID2ItemName[IDCount] = {'restaurant': restaurant, 'item': item, 'price': price}
            IDCount += 1
            for c in genAllGram(item):
                arr = menuIndex.get(c, dict())
                arr[itemName2ID[restaurant+item]] = True
                menuIndex[c] = arr

    # print(idx)

    query = '飯'

    for k in menuIndex[query]:
        print(ID2ItemName[k])

    obj = dict()
    obj['menu_index'] = menuIndex
    obj['item2iid'] = itemName2ID
    obj['iid2item'] = ID2ItemName

    import pickle
    pickle.dump(obj, open('menu_index.pkl', 'wb'))
    json.dump(obj, open('menu_index.json', 'w', encoding='UTF-8'), ensure_ascii=False)