import os, json, pickle
from index import travelMenu, genAllGram

inputDirPath = './json'
searchIndex = dict()
menuSearchIndex = dict()
restaurantInfoSearchIndex = dict()
restaurantID2Info = dict()
rid = 0
itemArr = list()
for dirPath, _, fileList in os.walk(inputDirPath):
    for fileName in fileList:
        fullPath = os.path.join(dirPath, fileName)
        restaurant_info = json.load(open(fullPath, 'r', encoding='UTF-8'))
        restaurantID2Info[rid] = restaurant_info
        itemArr.append(list())
        for item, price in travelMenu(str(), restaurant_info['root']):
            itemArr[rid].append(item)
            for seg in genAllGram(item):
                arr = menuSearchIndex.get(seg, dict())
                arr2 = arr.get(rid, dict())
                arr2[item] = True
                arr[rid] = arr2
                menuSearchIndex[seg] = arr
        for seg in genAllGram(restaurant_info['name']+restaurant_info['address']):
            arr = restaurantInfoSearchIndex.get(seg, dict())
            arr[rid] = True
            restaurantInfoSearchIndex[seg] = arr
        rid += 1
rtn = list()
for rrid in menuSearchIndex['薯條'].keys():
    r = restaurantID2Info[rrid]
    obj = {
        'name': r['name'], 
        'address': r['address'], 
        'phone': r['phone'], 
        'menu': list(menuSearchIndex['薯條'][rrid].keys())
    }
    rtn.append(obj)
print(rtn)

rtn = list()
for rrid in restaurantInfoSearchIndex['中正路']:
    r = restaurantID2Info[rrid]
    obj = {
        'name': r['name'], 
        'address': r['address'], 
        'phone': r['phone'], 
        'menu': itemArr[rrid]
    }
    rtn.append(obj)
print(rtn)

searchIndex['menuSearchIndex'] = menuSearchIndex
searchIndex['restaurantInfoSearchIndex'] = restaurantInfoSearchIndex
searchIndex['restaurantID2Info'] = restaurantID2Info

pickle.dump(searchIndex, open('./searchIndex.pkl', 'wb'))