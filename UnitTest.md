## OceanEat Django Run Unit Test
+ 輸入 `python3 manage.py shell` 開啟直譯互動環境
+ 輸入自動重新載入模組的指令，方便修改測試案例不須重開環境
  ```python
  %reload_ext autoreload
  %autoreload 2
  ```
+ 載入模組
  ```python
  from OceanEat import views
  ```
+ 執行 unittest
  ```python
  views.run_unittest()
  ```

## Write Unit Test
+ 寫法大致上與一般的 Unit Test 差不多
+ 比較特別的是大部份的 API 都需要傳入 Request 參數
+ 使用 SimulateRequest 類別建立虛擬 Request
  1. 先建立一個 dict 例如
    ```python
    request_body = {
        'user_name': 'Hayami',
        'mail_address': 'Hayami@gmail.com',
        'nick_name': 'LoveYouHayami',
        'gender': 'Woman',
        'phone_number': '0921354678',
        'psw': '123'
    }
    ```
  2. 再將 dict 以可變參數傳入建立 SimulateRequest 物件
    ```python
    request = SimulateRequest(**request_body)
    ```
  3. 先將預期的 Response 宣告好
    ```python
    response_success = response('Customer create success', 200, customer_id='5')
    ```
  4. 呼叫要測試的 API，大部分的 API 都是靜態函式，直接呼叫就好
    ```python
    self.assertEqual(CustomerViewSet.create_customer(request).content, response_success.content)
    ```
  5. 最後記得刪除建立的測試資料
    ```python
    requests.delete('https://nlp7.cse.ntou.edu.tw:8000/api/Customer/5/')
    ```