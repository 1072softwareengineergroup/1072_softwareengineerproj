"""Views of SearchIndex"""
from rest_framework import status, viewsets
from rest_framework.decorators import list_route
from rest_framework.response import Response

from SearchIndex.models import SearchIndex
from SearchIndex.serializers import SearchIndexSerializer


class SuperSearchHero():
    """Class of get search index in database."""
    def __init__(self):
        self.reload()

    def query(self, query):
        """Get result of query term."""
        rtn = list()
        arr = self.search_index['menuSearchIndex'].get(query, dict())
        for k in arr.keys():
            rtn.append(self.search_index['restaurantID2Info'][k])
        return rtn

    def reload(self):
        """[Deprecated] Try to reload self."""
        search_index = SearchIndex.objects.all()
        self.search_index = {
            'menuSearchIndex': search_index[0].search_index,
            'restaurantID2Info': search_index[1].search_index
        }

class SearchIndexViewSet(viewsets.ModelViewSet):
    """Default viewset of SearchIndex."""
    queryset = SearchIndex.objects.all()
    serializer_class = SearchIndexSerializer

    @staticmethod
    @list_route(methods=['get'])
    def query(request):
        """Get search result of query."""
        query_term = request.query_params.get('query', None)
        ssh = SuperSearchHero()
        final_index = list()

        def gen_ngram(term, num):
            """Generate ngram of term."""
            for i in range(len(term)-num+1):
                yield term[i:i+num]

        def gen_allgram(term):
            """Generate all possible ngram of term."""
            for num in range(len(term)):
                for result in gen_ngram(term, num+1):
                    yield result

        reverse_ngram = reversed([x for x in gen_allgram(query_term)])
        for sub_query in reverse_ngram:
            sub_result = ssh.query(sub_query)

            for result in sub_result:
                if result not in final_index:
                    final_index.append(result)

        return Response(final_index, status=status.HTTP_200_OK)

    @staticmethod
    @list_route(methods=['get'])
    def reload(request):
        """[Deprecated] Try to reload index."""
        return Response('ok', status=status.HTTP_200_OK)
