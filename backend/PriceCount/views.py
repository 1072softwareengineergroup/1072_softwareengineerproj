"""
Views of calculating price
"""
import math

from rest_framework import status, viewsets
from rest_framework.decorators import list_route
from rest_framework.response import Response

from PriceCount.models import PriceCount
from PriceCount.serializers import PriceCountSerializers


class CountOrderPrice(viewsets.ModelViewSet):
    """Default viewset of CountOrderPrice"""
    queryset = PriceCount.objects.all()
    serializer_class = PriceCountSerializers

    @staticmethod
    @list_route(methods=['get'], url_path='countPrice')
    def count_price(request):
        """Count price of order."""
        food_price_sum = request.query_params.get('foodprice', None)
        food_price_sum = int(food_price_sum)

        distance = request.query_params.get('distance', None)
        distance = float(distance)
        base_price = 33
        oe_coef = 1.0813
        distance_const = 0
        item_const = math.log10(food_price_sum)
        if distance < 1:
            distance_const = 1
        else:
            distance_const = 1.1 * distance

        orderprice = base_price * pow(oe_coef, distance_const * item_const)

        return Response(orderprice, status=status.HTTP_200_OK)
