"""
Register of OceanEat models.
"""

from django.contrib import admin

from OceanEat.models import (Customer, Delivery, Dishes, Order, Restaurant,
                             Restaurant_manager)

CLS = (Customer, Delivery, Dishes, Order, Restaurant, Restaurant_manager)
for c in CLS:
    admin.site.register(c)