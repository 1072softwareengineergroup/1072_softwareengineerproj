"""
API Views of project
"""
import datetime
import json
import math
import os
import random
import re
import smtplib
import unittest
from email.mime.text import MIMEText

import requests
from django.http import HttpResponse, JsonResponse
from django.utils.datastructures import MultiValueDictKeyError
from rest_framework import status, viewsets
from rest_framework.decorators import list_route
from rest_framework.response import Response
from webpush import send_group_notification
from SearchIndex.models import SearchIndex
from OceanEat.models import (Customer, Delivery, Dishes, Order, Restaurant,
                             Restaurant_manager)
from OceanEat.serializers import (CustomerSerializer, DeliverySerializer,
                                  DishesSerializer, OrderSerializer,
                                  RestaurantSerializer)


class RepeatRegistrationError(Exception):
    """Exception of repeat registration"""
    def __str__(self):
        return 'Repeat registration.'

class FormatError(Exception):
    """Abstract exception class of format error"""

class PhoneNumberFormatError(FormatError):
    """Exception of phone number format error"""
    def __str__(self):
        return 'Format of phone number is wrong.'

class EmailAddressFormatError(FormatError):
    """Exception of email address format error"""
    def __str__(self):
        return 'Format of email address is wrong.'

def parse_request(request, keys):
    """Get all field names in the request and make into object."""
    obj = dict()
    for k in keys:
        value = request.data.get(k, None)
        if value is not None:
            obj[k] = value
    return obj

def new_object_and_save(cls_name, args):
    """Create new object and save."""
    new_obj = cls_name(**args)
    new_obj.save()
    return new_obj

def new_customer(request):
    """Create new customer object."""
    uid = Customer.objects.count()
    keys = Customer.fields()
    obj = parse_request(request, keys)
    try:
        Customer.objects.get(mail_address=obj['mail_address'])
        raise RepeatRegistrationError()
    except Customer.DoesNotExist:
        pass
    validation_format(obj)
    obj['customer_id'] = str(uid)
    obj['is_valid'] = False
    return new_object_and_save(Customer, obj)

def update_user(request, uid):
    """Update user information"""
    user = Customer.objects.get(customer_id=uid)
    keys = Customer.fields()
    obj = parse_request(request, keys)
    validation_format(obj)
    for k in obj:
        setattr(user, k, obj[k])
    user.save()
    return response('Update done.', 200)

def update_customer(request):
    """Update customer object."""
    customer_id = request.data.get('customer_id', None)
    return update_user(request, customer_id)

def update_delivery(request):
    """Update delivery object."""
    delivery_id = request.data.get('delivery_id', None)
    return update_user(request, delivery_id)

def get_data_from_request_body(request_body, fields_name):
    """
    Get data from request.data or request.GET by fields name.
    """
    body = dict()
    for field in fields_name:
        body[field] = request_body.get(field, None)
    return body

def is_empty_in_body(body):
    """
    Return True if there is no None in values.
    """
    for key in body.keys():
        if body[key] is None:
            return False
    return True

def make_empty_field_msg(args):
    """
    Make a empty fields message.
    """
    if len(args) > 1:
        return 'Fields %s or %s is empty.' % (', '.join(args[:-1]), args[-1])

    return 'Filed %s is empty.' % args[0]

def response(msg, code, **kwargs):
    """
    Create a default HTTP code response.
    """
    response_map = {
        200: status.HTTP_200_OK,
        400: status.HTTP_400_BAD_REQUEST,
        404: status.HTTP_404_NOT_FOUND,
        406: status.HTTP_406_NOT_ACCEPTABLE
    }
    response_data = dict()
    response_data['status'] = (code == 200)
    response_data['msg'] = msg
    response_data = {**response_data, **kwargs}

    return JsonResponse(
        response_data,
        status=response_map.get(code, status.HTTP_500_INTERNAL_SERVER_ERROR))

def new_delivery(request):
    """Create new delivery object."""
    customer = new_customer(request)
    customer.delivery_mode = 1
    customer.save()
    obj = {'delivery_staff_id_id': customer.customer_id}
    return new_object_and_save(Delivery, obj)

def new_restaurant_manager(request):
    """Create new restaurant manger."""
    customer = new_customer(request)
    customer.delivery_mode = 2
    customer.save()
    obj = dict()
    obj['restaurant_manager_id'] = customer
    return new_object_and_save(Restaurant_manager, obj)

def calc_rank(rank, times):
    """Calculate rank"""
    if times == 0:
        return -1
    return rank / 2 / times

def validation_format(obj):
    """Validation of mail address & phone number"""
    validate_mail_address_format(obj.get('mail_address', 'abc@def.ghi'))
    validate_phone_number_format(obj.get('phone_number', '0987654321'))
    return True

def validate_phone_number_format(phone_number):
    """Validation of phone number format"""
    if not re.match(r'^\d{10,10}$', phone_number):
        raise PhoneNumberFormatError()
    return True

def validate_mail_address_format(mail_address):
    """Validation of mail address format"""
    if not re.match(r'^[^ ]+@[^ ]+$', mail_address):
        raise EmailAddressFormatError()
    return True

class CustomerViewSet(viewsets.ModelViewSet):
    """Default View Set of Customer."""
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer

    def list(self, request, **kwargs):
        users = Customer.objects.all()
        serializer = CustomerSerializer(users, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)

    @staticmethod
    @list_route(methods=['post'])
    def create_customer(request):
        """POST Method of create customer."""
        try:
            customer = new_customer(request)
            return response('Customer create success', 200, customer_id=customer.customer_id)
        except RepeatRegistrationError:
            return response('Repeat registration.', 406)
        except FormatError as err:
            return response(str(err), 400)

    @staticmethod
    @list_route(methods=['post'])
    def update_customer(request):
        """POST Method of update customer."""
        try:
            return update_customer(request)
        except FormatError as err:
            return response(str(err), 400)

    @staticmethod
    @list_route(methods=['post'])
    def do_rank(request):
        """POST Method of ranking customer"""
        fields_name = ('customer_id', 'rank')
        body = get_data_from_request_body(request.data, fields_name)
        if is_empty_in_body(body):
            try:
                user = Customer.objects.get(customer_id=body['customer_id'])
                user.custumer_rank += int(body['rank'])
                user.custumer_rank_times += 1
                user.save()
                return response('Rank success.', 200)
            except Customer.DoesNotExist:
                return response('User not found', 404)
        return response(make_empty_field_msg(fields_name), 400, fields=body)

    @staticmethod
    @list_route(methods=['get'])
    def get_rank(request):
        """Get rank of customer"""
        uid = request.data.get('customer_id', None)

        if not uid:
            return response('customer_id is required.', 400)

        try:
            user = Customer.objects.get(customer_id=uid)
            rank = user.custumer_rank
            times = user.custumer_rank_times
            result = calc_rank(rank, times)
            if result >= 0:
                return response('%.1f' % result, 200)
            return response('Unranked.', 200)
        except Customer.DoesNotExist:
            return response('Customer is not exist.', 404)

    @staticmethod
    @list_route(methods=['post'])
    def switch_mode(request):
        """POST Method of switch mode"""
        mode = ['customer', 'delivery']
        uid = request.data.get('customer_id', None)

        if not uid:
            return response('customer_id is required.', 400)

        try:
            user = Customer.objects.get(customer_id=uid)
            if user.delivery_mode == 2:
                return response('Restaurant manager is not able to switch mode.', 406)
            user.delivery_mode = (user.delivery_mode + 1) % 2
            user.save()
            return response(
                'User is switch to %s mode' % mode[user.delivery_mode], 200,
                mode=mode[user.delivery_mode])
        except Customer.DoesNotExist:
            return response('Customer is not exist.', 404)

    @staticmethod
    @list_route(methods=['post'])
    def update_picture(request):
        """POST Method of update picture"""
        try:
            pic = request.FILES["image"]
            if not 'image' in pic.content_type:
                return response('File upload is not image', 400)
            pic = pic.read()
        except MultiValueDictKeyError:
            return response('Field image is empty.', 400)
        uid = request.data.get('customer_id', None)
        if uid is None:
            return response('Field customer_id is empty.', 400)
        if not os.path.exists('./pic/'):
            os.mkdir('./pic/')
        with open('./pic/%s.png' % uid, 'wb') as fout:
            fout.write(pic)
        return response('ok', 200)

    @staticmethod
    @list_route(methods=['get'])
    def get_picture(request):
        """GET Method of get pictures"""
        uid = request.GET.get('customer_id')
        try:
            with open('./pic/%s.png' % uid, "rb") as fin:
                return HttpResponse(
                    fin.read(), content_type="image/png",
                    status=status.HTTP_200_OK)
        except FileNotFoundError:
            try:
                with open('./pic/default.png', 'rb') as fin:
                    return HttpResponse(
                        fin.read(), content_type="image/png",
                        status=status.HTTP_200_OK)
            except FileNotFoundError:
                if not os.path.exists('./pic/'):
                    os.mkdir('./pic/')
                with open('./pic/default.png', 'wb') as fout:
                    fout.write(requests.get('https://i.imgur.com/WuRgWsE.png').content)
        with open('./pic/default.png', 'rb') as fin:
            return HttpResponse(
                fin.read(), content_type="image/png",
                status=status.HTTP_200_OK)

class DeliveryViewSet(viewsets.ModelViewSet):
    """Default View Set of Delivery."""
    queryset = Delivery.objects.all()
    serializer_class = DeliverySerializer

    @staticmethod
    @list_route(methods=['post'])
    def create_delivery(request):
        """POST Method of create delivery."""
        try:
            delivery = new_delivery(request)
            return response(
                'Delivery create success', 200,
                delivery_id=delivery.delivery_staff_id_id)
        except RepeatRegistrationError:
            return response('Repeat registration', 406)
        except FormatError as err:
            return response(str(err), 400)

    @staticmethod
    @list_route(methods=['post'])
    def update_delivery(request):
        """POST Method of create delivery."""
        try:
            return update_delivery(request)
        except FormatError as err:
            return response(str(err), 400)

    @staticmethod
    @list_route(methods=['post'])
    def do_rank(request):
        """POST Method of ranking customer"""
        fields_name = ('delivery_id', 'rank')
        body = get_data_from_request_body(request.data, fields_name)
        if is_empty_in_body(body):
            try:
                user = Delivery.objects.get(delivery_staff_id_id=body['delivery_id'])
                user.delivery_staff_rank += int(body['rank'])
                user.delivery_staff_rank_time += 1
                user.save()
                return response('Rank success.', 200)
            except Customer.DoesNotExist:
                return response('Delivery not found', 404)
        return response(make_empty_field_msg(fields_name), 400, fields=body)

    @staticmethod
    @list_route(methods=['get'])
    def get_rank(request):
        """Get rank of customer"""
        uid = request.data.get('delivery_id', None)

        if not uid:
            return response('delivery_id is required.', 400)

        try:
            user = Delivery.objects.get(delivery_staff_id_id=uid)
            rank = user.delivery_staff_rank
            times = user.delivery_staff_rank_time
            result = calc_rank(rank, times)
            if result >= 0:
                return response('%.1f' % result, 200)
            return response('Unranked.', 200)
        except Delivery.DoesNotExist:
            return response('Delivery is not exist.', 404)

class RestaurantManagerViewSet(viewsets.ModelViewSet):
    """Default View Set of RestaurantManager"""
    queryset = Restaurant_manager.objects.all()
    serializer_class = DeliverySerializer

    @staticmethod
    @list_route(methods=['post'])
    def create_restaurant_manager(request):
        """POST Method of create restaurant manager."""
        rmm_id = 'restaurant_manager_id_id'
        try:
            rmm = new_restaurant_manager(request)
        except RepeatRegistrationError:
            return response('Repeat registration', 406)
        except FormatError as err:
            return response(str(err), 400)
        kwargs = {'restaurant_manager_id': getattr(rmm, rmm_id)}
        return response('Restaurant manager create success', 200, **kwargs)

class RestaurantViewSet(viewsets.ModelViewSet):
    """Default View Set of Restaurant."""
    queryset = Restaurant.objects.all()
    serializer_class = RestaurantSerializer

    @staticmethod
    @list_route(methods=['post'])
    def create_restaurant(request):
        """Create new restaurant"""
        fields_name = (
            'address', 'lacation', 'mail_address',
            'phone_number', 'restaurant_name', 'restaurant_owner')
        body = get_data_from_request_body(request.data, fields_name)
        if is_empty_in_body(body):
            rmg = Restaurant_manager.objects.get(restaurant_manager_id_id=body['restaurant_owner'])
            body['restaurant_owner'] = rmg
            restaurant = Restaurant()
            rid = Restaurant.objects.count()
            while Restaurant.objects.filter(restaurant_id=rid).exists():
                rid += 1
            restaurant.restaurant_id = rid
            for field in fields_name:
                setattr(restaurant, field, body[field])
            restaurant.save()
            return response('Restaurant create success.', 200)
        return response(make_empty_field_msg(fields_name), 400, fields=body)

    @staticmethod
    @list_route(methods=['post'])
    def update_restaurant(request):
        """Update restaurant"""
        fields_name = (
            'address', 'lacation', 'mail_address',
            'phone_number', 'restaurant_name', 'restaurant_id')
        body = get_data_from_request_body(request.data, fields_name)
        if is_empty_in_body(body):
            restaurant = Restaurant.objects.get(restaurant_id=body['restaurant_id'])
            for field in fields_name[:-1]:
                setattr(restaurant, field, body[field])
            restaurant.save()
            return response('Restaurant update success.', 200)
        return response(make_empty_field_msg(fields_name), 400, fields=body)

    @staticmethod
    @list_route(methods=['post'])
    def update_restaurant_dishes(request):
        """Update dishes of restaurant"""
        data = json.loads(request.body)

        restaurant = Restaurant.objects.get(restaurant_name=data['name'])
        rid = restaurant.restaurant_id

        menu = data['root']
        dishes = Dishes.objects.filter(restaurant_id_id=rid)

        old_menu = dict()
        for dish in dishes:
            old_menu[dish.dishes_name] = dish
            dish.option = {'disable': True}

        for cat in menu:
            did = Dishes.objects.count()
            while Dishes.objects.filter(dishes_id=did).exists():
                did += 1
            dish = old_menu.get(
                cat, Dishes(
                    dishes_name=cat, restaurant_id=restaurant, dishes_price=0, dishes_id=did))

            if isinstance(menu[cat], dict):
                dish.option = menu[cat]
            elif isinstance(menu[cat], int):
                dish.dishes_price = menu[cat]
                dish.option = {}
            dish.save()

        for dish in dishes:
            if dish.option == {'disable': True}:
                dish.delete()

        return response('ok', 200)

    @staticmethod
    @list_route(methods=['get'])
    def get_restaurant(request):
        """Get restaurant list by restaurant manager id"""
        rmid = request.GET.get('rmid', None)
        if rmid is None:
            return response('Filed rmid is required.', 400)
        rmid = Restaurant_manager.objects.get(restaurant_manager_id_id=rmid)
        restaurants = Restaurant.objects.filter(restaurant_owner=rmid)
        result = {'restaurants': []}
        for restaurant in restaurants:
            result['restaurants'].append(restaurant.parse())
        return JsonResponse(result, status=status.HTTP_200_OK)

    @staticmethod
    @list_route(methods=['post'])
    def update_picture(request):
        """Update picture of restaurant"""
        try:
            pic = request.FILES["image"]
            if not 'image' in pic.content_type:
                return response('File upload is not image', 400)
            pic = pic.read()
        except MultiValueDictKeyError:
            return response('Field image is empty.', 400)
        uid = request.data.get('rid', None)
        if uid is None:
            return response('Field rid is empty.', 400)
        if not os.path.exists('./pic/'):
            os.mkdir('./pic/')
        with open('./pic/rm%s.png' % uid, 'wb') as fout:
            fout.write(pic)
        return response('ok', 200)

    @staticmethod
    @list_route(methods=['get'])
    def get_picture(request):
        """GET Method of get pictures"""
        uid = request.GET.get('rid')
        try:
            with open('./pic/rm%s.png' % uid, "rb") as fin:
                return HttpResponse(
                    fin.read(), content_type="image/png",
                    status=status.HTTP_200_OK)
        except FileNotFoundError:
            try:
                with open('./pic/rm_default.png', 'rb') as fin:
                    return HttpResponse(
                        fin.read(), content_type="image/png",
                        status=status.HTTP_200_OK)
            except FileNotFoundError:
                if not os.path.exists('./pic/'):
                    os.mkdir('./pic/')
                with open('./pic/rm_default.png', 'wb') as fout:
                    fout.write(requests.get('https://via.placeholder.com/300').content)
        with open('./pic/rm_default.png', 'rb') as fin:
            return HttpResponse(
                fin.read(), content_type="image/png",
                status=status.HTTP_200_OK)

    @staticmethod
    @list_route(methods=['get'])
    def get_dishes_list(request):
        """Get dishes list of restaurant"""
        rid = request.GET.get('rid', None)
        if rid is None:
            return response('Field rid is required.', 400)
        dishes = Dishes.objects.filter(restaurant_id_id=rid)
        dishes_contents = {'dishes': []}
        for dish in dishes:
            for item, price in dish.parse():
                dishes_contents['dishes'].append({'item': item, 'price': price})
        return JsonResponse(dishes_contents, status=status.HTTP_200_OK)

    @staticmethod
    @list_route(methods=['get'])
    def get_nearby_restaurant(request):
        """Get nearby restaurant by location"""
        user_x = float(request.query_params.get('x', None))
        user_y = float(request.query_params.get('y', None))
        restaurant_list = Restaurant.objects.all()
        nearby_list = list()
        for restaurant in restaurant_list:
            try:
                target_x, target_y = restaurant.lacation.split(', ')
                target_x = float(target_x)
                target_y = float(target_y)
                dist = ((user_x - target_x) ** 2 + (user_y - target_y) ** 2) ** 0.5
                dist = math.log(dist)
                if dist < -3:
                    nearby_list.append(restaurant.restaurant_id)
            except ValueError:
                print('Restaurant %s has invalid location.' % restaurant)
        return JsonResponse({'restaurant': nearby_list}, status=status.HTTP_200_OK)

    @staticmethod
    @list_route(methods=['get'])
    def update_search_index(request):
        """Update search index."""
        restaurants_list = Restaurant.objects.all()
        rid2info = dict()
        for restaurant in restaurants_list:
            rid = restaurant.restaurant_id
            rid2info[rid] = restaurant.parse()
            dishes = Dishes.objects.filter(restaurant_id_id=rid)
            dishes_contents = {'dishes': []}
            for dish in dishes:
                for item, price in dish.parse():
                    dishes_contents['dishes'].append({'item': item, 'price': price})
            menu = dict()
            for dish in dishes_contents['dishes']:
                dish_dict = menu.get(dish['item'][0], dict())
                if len(dish['item']) > 1:
                    another = dish_dict
                    for i in dish['item'][1:-1]:
                        dish_dict_path = dish_dict.get(i, dict())
                        dish_dict[i] = dish_dict_path
                        dish_dict = dish_dict[i]
                    dish_dict[dish['item'][-1]] = dish['price']
                    menu[dish['item'][0]] = another
                else:
                    menu[dish['item'][0]] = dish['price']
            rid2info[rid]['menu'] = menu

        def travel_menu(item_name, menu):
            """Travel the menu dict."""
            if not isinstance(menu, dict):
                yield (item_name, menu)
                return

            for key in menu.keys():
                for item in travel_menu(item_name + key, menu[key]):
                    yield item

        def gen_ngram(term, num):
            """Generate ngram of term."""
            for i in range(len(term)-num+1):
                yield term[i:i+num]

        def gen_allgram(term):
            """Generate all possible ngram of term."""
            for num in range(len(term)):
                for result in gen_ngram(term, num+1):
                    yield result

        search_index = dict()
        for restaurant in rid2info:
            for item, price in travel_menu(str(), rid2info[restaurant]['menu']):
                for seg in gen_allgram(item):
                    arr = search_index.get(seg, dict())
                    arr[restaurant] = True
                    search_index[seg] = arr

            for seg in gen_allgram(rid2info[restaurant]['restaurant_name']):
                arr = search_index.get(seg, dict())
                arr[restaurant] = True
                search_index[seg] = arr

            for seg in gen_allgram(rid2info[restaurant]['address']):
                arr = search_index.get(seg, dict())
                arr[restaurant] = True
                search_index[seg] = arr

        SearchIndex(id=1, search_index=search_index).save()
        SearchIndex(id=2, search_index=rid2info).save()

        return response('ok', 200)

class DishesViewSet(viewsets.ModelViewSet):
    """Default View Set of Dishes."""
    queryset = Dishes.objects.all()
    serializer_class = DishesSerializer

class OrderViewSet(viewsets.ModelViewSet):
    """Default View Set of Order."""
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    ORDER_CREATE = 0 # 新建立的訂單
    ORDER_ACCEPT = 1 # 外送中的訂單
    ORDER_DONE_BY_USER = 2 # 使用者已確認送達
    ORDER_DONE_BY_DELIVERY = 3 # 外送員已確認送達
    ORDER_FINISH = 4 # 訂單已被雙方確認
    ORDER_DELETE = 5 # 訂單已被刪除

    @staticmethod
    @list_route(methods=['post'])
    def create_order(request):
        """
        POST Method of create new order.
        """
        fields_name = ('customer_id', 'order_items', 'dest', 'price')
        body = get_data_from_request_body(request.data, fields_name)
        if is_empty_in_body(body):
            order_id = datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")
            Order(
                order_id=order_id,
                customer_id=body['customer_id'],
                order_items=json.loads(body['order_items']),
                destination=body['dest'],
                status=OrderViewSet.ORDER_CREATE,
                price=body['price']
            ).save()
            return response('Order create success.', 200, order_id=order_id)
        return response(make_empty_field_msg(fields_name), 400, fields=body)

    @staticmethod
    @list_route(methods=['post'])
    def accept_order(request):
        """
        POST Method of accept order.
        """
        fields_name = ('delivery_id', 'order_id')
        body = get_data_from_request_body(request.data, fields_name)
        if is_empty_in_body(body):
            try:
                order = Order.objects.get(order_id=body['order_id'])
                order.delivery_id = body['delivery_id']
                if order.status == OrderViewSet.ORDER_CREATE:
                    order.status = OrderViewSet.ORDER_ACCEPT
                    order.save()
                    return response('Order accept success.', 200, order_id=body['order_id'])
                return response(
                    'Order is not acceptable.',
                    400, order_id=body['order_id'],
                    order_status=order.status)
            except Order.DoesNotExist:
                return response('Order is not exist.', 400, order_id=body['order_id'])

        return response(make_empty_field_msg(fields_name), 400, fileds=body)

    @staticmethod
    @list_route(methods=['post'])
    def finish_order(request):
        """
        POST Method of finish order.
        """
        fields_name = ('order_id', 'acceptor')
        body = get_data_from_request_body(request.data, fields_name)
        if is_empty_in_body(body):
            try:
                order = Order.objects.get(order_id=body['order_id'])
                kwargs = {'order_id': body['order_id']}
                if order.status == OrderViewSet.ORDER_ACCEPT:
                    if body['acceptor'] == 'user':
                        order.status = OrderViewSet.ORDER_DONE_BY_USER
                    elif body['acceptor'] == 'delivery':
                        order.status = OrderViewSet.ORDER_DONE_BY_DELIVERY
                    order.save()
                    return response('Order is done by one.', 200, **kwargs)
                if order.status == OrderViewSet.ORDER_DONE_BY_USER:
                    if body['acceptor'] == 'delivery':
                        order.status = OrderViewSet.ORDER_FINISH
                        order.save()
                        return response('Order is completely finish.', 200, **kwargs)
                if order.status == OrderViewSet.ORDER_DONE_BY_DELIVERY:
                    if body['acceptor'] == 'user':
                        order.status = OrderViewSet.ORDER_FINISH
                        order.save()
                        return response('Order is completely finish.', 200, **kwargs)
                return response('Order is not able to finish.', 400, **kwargs)
            except Order.DoesNotExist:
                return response('Order is not exist.', 400, order_id=body['order_id'])

        return response(make_empty_field_msg(fields_name), 400, fileds=body)

    @staticmethod
    @list_route(methods=['post'])
    def delete_order(request):
        """
        POST Method of delete order.
        """
        fields_name = ('order_id', )
        body = get_data_from_request_body(request.data, fields_name)
        if is_empty_in_body(body):
            try:
                order = Order.objects.get(order_id=body['order_id'])
                if order.status == OrderViewSet.ORDER_CREATE:
                    order.status = OrderViewSet.ORDER_DELETE
                    order.save()
                    return response('Order delete success.', 200, order_id=body['order_id'])
                return response('Order is not able to delete.', 400, order_id=body['order_id'])
            except Order.DoesNotExist:
                return response('Order is not exist.', 400, order_id=body['order_id'])

        return response(make_empty_field_msg(fields_name), 400, fileds=body)

    @staticmethod
    def parse_order(orders):
        """
        Parse order info into dictionary.
        """
        order_list = list()
        for order in orders:
            order_json = dict()
            order_json['order_id'] = order.order_id
            order_json['customer_id'] = order.customer_id
            if order.delivery_id:
                order_json['delivery_id'] = order.delivery_id
            order_json['order_items'] = order.order_items
            order_json['destination'] = order.destination
            order_json['status'] = order.status
            order_json['price'] = order.price
            order_list.append(order_json)
        return order_list

    @staticmethod
    @list_route(methods=['get'])
    def query_order(request):
        """
        GET Method of get orders by customer id.
        """
        customer_id = request.GET.get('customer_id', None)
        orders = Order.objects.filter(customer_id=customer_id)
        order_list = OrderViewSet.parse_order(orders)
        return JsonResponse({'orders': order_list}, status=status.HTTP_200_OK)

    @staticmethod
    @list_route(methods=['get'])
    def query_order_by_delivery(request):
        """
        GET Method of get orders by delivery id.
        """
        delivery_id = request.GET.get('delivery_id', None)
        orders = Order.objects.filter(delivery_id=delivery_id)
        order_list = OrderViewSet.parse_order(orders)
        return JsonResponse({'orders': order_list}, status=status.HTTP_200_OK)

    @staticmethod
    @list_route(methods=['get'])
    def query_unassigned_order(request):
        """
        GET Method of get unassigned orders.
        """
        request.session['query'] = 1
        orders = Order.objects.filter(status=0)
        order_list = OrderViewSet.parse_order(orders)
        return JsonResponse({'orders': order_list}, status=status.HTTP_200_OK)

def test_operation(request):
    """
    Test operation of session testing.
    """
    print(dir(request.session))
    print(request.session['username'])
    user_name = request.session['username']
    if not user_name:
        return response("Not login", 500)
    user = Customer.objects.get(user_name=user_name)
    return response(user.mail_address, 200)

def login(request):
    """POST Method of login."""
    obj = json.loads(request.body.decode('UTF-8'))
    login_name = obj['username']
    login_password = obj['password']
    try:
        user = Customer.objects.get(mail_address=login_name)
        if not user.is_valid:
            return response('User is not verified.', 406)
        if user.psw == login_password:
            request.session['username'] = user.user_name
            request.session['useremail'] = user.mail_address
            user.is_online = True
            user.save()
            return response('Login success by ' + user.user_name, 200)
        return response('Wrong Password.', 406)
    except Customer.DoesNotExist:
        return response('User not found.', 404)

def is_login(request):
    """
    GET Method that if user is login in session.
    """
    mode = {
        0: 'customer',
        1: 'delivery',
        2: 'restaurant manager'
    }
    if 'username' in request.session and request.session['useremail'] is not None:
        try:
            user = Customer.objects.get(mail_address=request.session['useremail'])
            if user.is_online:
                return response(
                    'User is login.', 200,
                    user_id=user.customer_id,
                    user_type=mode[user.delivery_mode])
            return response('User is not login.', 400)
        except Customer.DoesNotExist:
            return response('User not found.', 404)
    return response('Session is not login.', 400)

def logout(request):
    """POST Method of logout."""
    if 'username' in request.session:
        try:
            user = Customer.objects.get(mail_address=request.session['useremail'])
            request.session['username'] = None
            request.session['useremail'] = None
            user.is_online = False
            user.save()
            return response('Logout success.', 200)
        except Customer.DoesNotExist:
            return response('User not found.', 404)
    return response('Is not login.', 400)

def mailcheck(request):
    """
    Send verify code to user mail via gmail.
    """
    if request.method != 'POST':
        return response('This url is only allow with POST method.', 405)

    verify = random.randint(100000, 999999)
    gmail_settings = json.load(open('./backend/verify_mail.json', 'r'))
    gmail_user = gmail_settings['mail']
    gmail_password = gmail_settings['password']
    reciver = request.POST.get("user_mail", None)
    user_name = request.POST.get("user_name", None)

    if not reciver:
        return response("Field user_mail is empty.", 400)
    if not user_name:
        return response("Field user_name is empty.", 400)

    msg = MIMEText('海大餐飲系統驗證信：\n驗證碼：%s' % str(verify))
    msg['Subject'] = "海大餐飲系統驗證信"
    msg['From'] = gmail_user
    msg['To'] = reciver
    server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
    server.ehlo()
    server.login(gmail_user, gmail_password)
    server.send_message(msg)
    server.quit()

    user = Customer.objects.get(mail_address=reciver)
    user.validation_code = str(verify)
    user.save()

    return response("Verify email send.", 200)

def verify_validation_code(request):
    """
    POST Method of verify validation code.
    """
    user_mail = request.POST.get("user_mail", None)
    if not user_mail:
        return response("Field user_mail is empty.", 400)
    req_validation_code = request.POST.get("code", None)
    customer = Customer.objects.get(mail_address=user_mail)
    db_validation_code = customer.validation_code

    if req_validation_code == db_validation_code:
        customer.is_valid = 1
        customer.save()
        return response("Success", 200)
    return response("Validation code incorrect", 406)

def send_push(request):
    """Send web push notification"""
    try:
        payload = {
            'notification': {
                'title': '有新的外送訂單!!',
                "actions": [{
                    "action": "ShowOrderPage",
                    "title": "前往訂單頁面 >"
                }], "icon": "assets/icons/icon-512x512.png",
                "body": "海大餐飲外送系統 - 推播通知",
                "data": {"url": "https://nlp7.cse.ntou.edu.tw/orders/"}
            }
        }
        send_group_notification(group_name="delivery", payload=payload, ttl=1000)
        return JsonResponse(status=200, data={"message": "Web push successful"})
    except TypeError:
        return JsonResponse(status=500, data={"message": "An error occurred"})

class SimulateRequest(): # pylint: disable=too-few-public-methods
    """Simulation of http request"""
    def __init__(self, **kwargs):
        self.data = kwargs
        self.POST = kwargs # pylint: disable=invalid-name
        self.GET = kwargs  # pylint: disable=invalid-name

class TestCase(unittest.TestCase):
    """Unit test of api"""

    def test_create(self):
        """Test case of creation"""
        request_body = {
            'user_name': 'Hayami',
            'mail_address': 'Hayami@gmail.com',
            'nick_name': 'LoveYouHayami',
            'gender': 'Woman',
            'phone_number': '0921354678',
            'psw': '123'
        }
        request = SimulateRequest(**request_body)
        response_success = response('Customer create success', 200, customer_id='5')
        response_repeat = response('Repeat registration.', 406)

        self.assertEqual(CustomerViewSet.create_customer(request).content, response_success.content)
        self.assertEqual(CustomerViewSet.create_customer(request).content, response_repeat.content)
        requests.delete('https://nlp7.cse.ntou.edu.tw:8000/api/Customer/5/')

def run_unittest():
    """Hand make unittest main"""
    test_case = TestCase()
    test_case.test_create()
    print('Test Success')

@list_route(methods=['get'], url_path='version')
def version(request):
    """Get version of API"""
    return HttpResponse('0.9.8', status=status.HTTP_200_OK)
