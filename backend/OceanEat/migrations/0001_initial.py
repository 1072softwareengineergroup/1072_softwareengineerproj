# Generated by Django 2.1.7 on 2019-05-19 09:16

from django.db import migrations, models
import django.db.models.deletion
import django_mysql.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('customer_id', models.CharField(max_length=40, primary_key=True, serialize=False)),
                ('mail_address', models.CharField(max_length=150)),
                ('psw', models.CharField(max_length=20)),
                ('user_name', models.CharField(max_length=20)),
                ('nick_name', models.CharField(max_length=20, null=True)),
                ('gender', models.CharField(max_length=10, null=True)),
                ('phone_number', models.CharField(max_length=20)),
                ('address', models.CharField(max_length=250, null=True)),
                ('discribe', models.TextField(null=True)),
                ('other_contact', django_mysql.models.JSONField(default=dict, null=True)),
                ('custumer_rank', models.IntegerField(default=0, null=True)),
                ('custumer_rank_times', models.IntegerField(default=0, null=True)),
                ('delivery_mode', models.BooleanField(default=0)),
                ('validation_code', models.CharField(max_length=70, null=True)),
                ('is_online', models.BooleanField(default=False)),
            ],
            options={
                'db_table': 'customer',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Delivery_staff',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('delivery_staff_rank', models.IntegerField(default=0)),
                ('delivery_staff_rank_time', models.IntegerField(default=0)),
                ('delivery_staff_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='OceanEat.Customer')),
            ],
            options={
                'db_table': 'delivery_staff',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Dishes',
            fields=[
                ('dishes_id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('dishes_name', models.CharField(max_length=45)),
                ('dishes_price', models.IntegerField()),
                ('option', django_mysql.models.JSONField(default=dict, null=True)),
            ],
            options={
                'db_table': 'dishes',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('order_id', models.CharField(max_length=40, primary_key=True, serialize=False)),
                ('customer_id', models.CharField(max_length=40)),
                ('delivery_id', models.CharField(max_length=40)),
                ('order_items', django_mysql.models.JSONField(default=dict)),
                ('destination', models.CharField(max_length=250)),
                ('status', models.IntegerField()),
                ('price', models.IntegerField()),
            ],
            options={
                'db_table': 'order',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Restaurant',
            fields=[
                ('restaurant_id', models.CharField(max_length=40, primary_key=True, serialize=False)),
                ('restaurant_name', models.CharField(max_length=45)),
                ('phone_number', models.CharField(max_length=20)),
                ('address', models.CharField(max_length=250)),
                ('mail_address', models.CharField(max_length=150, null=True)),
                ('lacation', models.CharField(max_length=45)),
                ('total_rank', models.IntegerField(default=0)),
                ('rank_times', models.IntegerField(default=0)),
            ],
            options={
                'db_table': 'restaurant',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Restaurant_manager',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('restaurant_manager_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='OceanEat.Customer')),
            ],
            options={
                'db_table': 'restaurant_manager',
                'managed': True,
            },
        ),
        migrations.AddField(
            model_name='restaurant',
            name='restaurant_owner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='OceanEat.Restaurant_manager'),
        ),
        migrations.AddField(
            model_name='dishes',
            name='restaurant_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='OceanEat.Restaurant'),
        ),
    ]
