"""
OceanEat Models
"""
import json

from django.db import models
from django_mysql.models import JSONField


class Customer(models.Model):
    customer_id = models.CharField(max_length=40, primary_key=True)
    mail_address = models.CharField(max_length=150)
    psw = models.CharField(max_length=80)
    user_name = models.CharField(max_length=20)
    nick_name = models.CharField(max_length=20, null=True)
    gender = models.CharField(max_length=10, null=True)
    phone_number = models.CharField(max_length=20)
    address = models.CharField(max_length=250, null=True)
    discribe = models.TextField(null=True)
    other_contact = JSONField(null=True)
    custumer_rank = models.IntegerField(null=True, default=0)
    custumer_rank_times = models.IntegerField(null=True, default=0)
    delivery_mode = models.IntegerField(default=0)
    validation_code = models.CharField(max_length=70, null=True)
    is_online = models.BooleanField(default=False)
    is_valid = models.BooleanField(default=False)

    @staticmethod
    def fields():
        return [
            'mail_address', 'psw', 'user_name', 'nick_name',
            'gender', 'phone_number', 'address', 'discribe', 'other_contact'
        ]

    class Meta:
        managed = True
        db_table = 'customer'

    def __str__(self):
        return '%s <%s>' % (self.user_name, self.mail_address)

class Delivery(models.Model):
    """Model of Delivery Staff"""
    delivery_staff_id = models.ForeignKey(Customer, models.CASCADE)
    delivery_staff_rank = models.IntegerField(default=0)
    delivery_staff_rank_time = models.IntegerField(default=0)

    class Meta:
        managed = True
        db_table = 'delivery_staff'

    def __str__(self):
        return '%s' % self.delivery_staff_id

class Restaurant_manager(models.Model): # !pylint: disable=invalid-name
    """Model of Restaurant Manager"""
    restaurant_manager_id = models.ForeignKey(Customer, models.CASCADE)

    class Meta:
        managed = True
        db_table = 'restaurant_manager'

class Restaurant(models.Model):
    """Model of Restaurant"""
    restaurant_id = models.CharField(max_length=40, primary_key=True)
    restaurant_owner = models.ForeignKey(Restaurant_manager, models.CASCADE)
    restaurant_name = models.CharField(max_length=45)
    phone_number = models.CharField(max_length=20)
    address = models.CharField(max_length=250)
    mail_address = models.CharField(max_length=150, null=True)
    lacation = models.CharField(max_length=45)
    total_rank = models.IntegerField(default=0)
    rank_times = models.IntegerField(default=0)
    available = models.BooleanField(default=1)

    def parse(self):
        """Parsing restaurant info into dict"""
        info = dict()
        fields = (
            'restaurant_name', 'phone_number', 'address',
            'lacation', 'mail_address', 'restaurant_id')
        for field in fields:
            info[field] = getattr(self, field)
        return info

    class Meta:
        managed = True
        # unique_together = ['restaurant_id', 'restaurant_name']
        db_table = 'restaurant'

    def __str__(self):
        return self.restaurant_name

class Dishes(models.Model):
    """Model of Dishes"""
    dishes_id = models.CharField(max_length=20, primary_key=True)
    restaurant_id = models.ForeignKey(Restaurant, models.CASCADE)
    dishes_name = models.CharField(max_length=45)
    dishes_price = models.IntegerField()
    option = JSONField(null=True)

    class Meta:
        managed = True
        db_table = 'dishes'

    def travel(self, data, str_list):
        """Travel throught the dict"""
        if isinstance(data, int):
            yield (str_list, data)
            return
        for k in data:
            for result in self.travel(data[k], str_list+[k]):
                yield result

    def parse(self):
        """Parsing dishes info into dict"""
        if self.option == {}:
            return [([self.dishes_name], self.dishes_price)]
        if isinstance(self.option, str):
            data = json.loads(self.option)
        elif isinstance(self.option, dict):
            data = self.option
        return self.travel(data, [self.dishes_name])

    def __str__(self):
        return '[%s] %s <%s>' % (self.dishes_id, self.restaurant_id, self.dishes_name)

class Order(models.Model):
    """Model of Order"""
    order_id = models.CharField(max_length=40, primary_key=True)
    customer_id = models.CharField(max_length=40)
    delivery_id = models.CharField(max_length=40, null=True)
    order_items = JSONField()
    destination = models.CharField(max_length=250)
    status = models.IntegerField()
    price = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'order'

    def __str__(self):
        desc = [
            '新建立的訂單',
            '外送中的訂單',
            '使用者已確認送達',
            '外送員已確認送達',
            '訂單已被雙方確認',
            '訂單已被刪除'
        ]

        return '[%s] %s' % (self.order_id, desc[self.status])
