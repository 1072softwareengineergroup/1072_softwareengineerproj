"""
Django settings for backend project.
"""

import json
import os
from corsheaders.defaults import default_methods, default_headers
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'wb+f@kagx4lb(#wxs*a4c4ve-uro6^w*8dt(3k4rv@=b)@g#&='

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = [
    'nlp7.cse.ntou.edu.tw',
    'localhost',
    '127.0.0.1',
]

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'OceanEat',
    'sslserver',
    'corsheaders',
    'webpush'
]

INSTALLED_APPS.append('PriceCount.apps.PricecountConfig')
INSTALLED_APPS.append('SearchIndex.apps.SearchindexConfig')
INSTALLED_APPS.append('rest_framework')


MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.BrokenLinkEmailsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_CREDENTIALS = True

ROOT_URLCONF = 'backend.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'backend.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

TEST = True
# TEST = False
MYSQL_SEETING_JSON_PATH = 'backend/mysql_test.json' if TEST else 'backend/mysql_dev.json'

MYSQL_SEETING_JSON_PATH = os.path.join(BASE_DIR, MYSQL_SEETING_JSON_PATH)
DATABASES_SETTINGS = json.load(open(MYSQL_SEETING_JSON_PATH, 'r', encoding='UTF-8'))

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'OPTIONS': {'charset': 'utf8mb4'},
    }
}

for k in DATABASES_SETTINGS.keys():
    DATABASES['default'][k] = DATABASES_SETTINGS[k]

# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

WEBPUSH_SETTINGS = {
    "VAPID_PUBLIC_KEY":
        "BFecJLI3KQfgioH1_lVJkdJKP9Gcw_jKzGbdi-e3_-lTUwmghLw7RdKxrxH8mC1Kk24J85JvHrS1zAz58GhwCi4",
    "VAPID_PRIVATE_KEY": "Ys7qq8efGZmZjyGkjTlXKKPk95TnG0YxI5ATBHG-KVQ",
    "VAPID_ADMIN_EMAIL": "Oceaneat1072@gmail.com"
}

# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/

LANGUAGE_CODE = 'zh-tw'

TIME_ZONE = 'Asia/Taipei'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

STATIC_URL = '/static/'
