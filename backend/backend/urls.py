"""
backend URL Configuration
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.urls import path
from rest_framework.routers import DefaultRouter

import PriceCount.views
import SearchIndex.views
from OceanEat import views

ROUTER = DefaultRouter()

# register OceanEat ViewSet (API)
ROUTER.register(r'Customer', views.CustomerViewSet)
ROUTER.register(r'Delivery', views.DeliveryViewSet)
ROUTER.register(r'RestaurantManager', views.RestaurantManagerViewSet)
ROUTER.register(r'Restaurant', views.RestaurantViewSet)
ROUTER.register(r'Dishes', views.DishesViewSet)
ROUTER.register(r'Order', views.OrderViewSet)
ROUTER.register(r'SearchIndex', SearchIndex.views.SearchIndexViewSet)
ROUTER.register(r'PriceCount', PriceCount.views.CountOrderPrice)

# For APIs
urlpatterns = [
    url(r'^api/', include(ROUTER.urls)),
    path('login/', views.login),
    path('is_login/', views.is_login),
    path('logout/', views.logout),
    path('mailcheck/', views.mailcheck),
    path('verify_validation_code/', views.verify_validation_code),
    path('test_operation/', views.test_operation),
    path('send_push/', views.send_push),
    path('webpush/', include('webpush.urls')),
    path('admin/', admin.site.urls),
    path('api/version/', views.version)
]
