# API Documents

[TOC]

[API Documents HackMD](https://hackmd.io/@PenutChen/rJv4zp2AV)

## Server
+ To run https server
  ```bash
  # python3 manage.py runsslserver 10.0.2.15:8000 --certificate /home/user/cert_file/cert.pem --key /home/user/cert_file/key.pem
  ```
+ To run http redirect
  ```bash
  # python3 -m http.server 80
  ```

## Database
+ DB Build
    ```
    python manage.py makemigrations
    python manage.py showmigrations
    python manage.py sqlmigrate OceanEat 0001_initial //0001_initial 要換成第2行指令顯示 OceanEat下的最後一個
    python manage.py migrate
    ```
+ import
    ```
    from OceanEat.models import Customer
    ```
+ create
    ```
    Customer.objects.create(customer_id='1',mail_address='10757013@ntou.edu.tw',psw='1111',user_name='alex',nick_name='sora',gender='男',phone_number='0912345678',address='基隆中正區北寧路',discribe='adad',other_contact='{}')
    ```
+ 以下來不及寫
    ```
    Customer.objects.all()
    for c in Customer.objects.all():\
    print(c.mail_address)

    c = Customer.objects.all()[0]
    c.save() //執行這行才會新增到MySQL
    ```

## 驗證
### 寄送驗證信
+ `/mailcheck/`
+ Method: POST
+ Body
  + user_mail: 完整個 Email
  + user_name: 使用者名稱
+ Return
  ```json
  {
    "status": true,
    "msg": "Verify email send."
  }
  ```

### 驗證碼確認
+ `/verify_validation_code/`
+ Method: POST
+ Body
  + user_mail: 使用者信箱
  + code: 使用者填入的驗證碼
+ Return
  + 驗證碼正確
    ```json
    {
      "status": true,
      "msg": "Success"
    }
    ```
  + 驗證碼錯誤
    ```json
    {
      "status": false,
      "msg": "Validation code incorrect"
    }
    ```

## 會員資料
### 創建消費者
+ `/api/Customer/create_customer/`
+ Method: POST
+ Body
  + mail_address 
  + psw
  + user_name 
  + nick_name 
  + gender 
  + phone_number 
  + address 
  + discribe 
  + other_contact 
  + custumer_rank 
  + custumer_rank_times 
  + delivery_mode

### 創建外送員
+ `/api/Delivery/create_delivery/`
+ Method: POST
+ Body
  + 同 customer
  + delivery_rank 
  + delivery_rank_time

### 創建餐廳管理員
+ `/api/RestaurantManager/create_restaurant_manager/`
+ Method: POST
+ Body
  + user_name: 負責人名稱
  + phone_number: 負責人聯絡電話
  + address: 地址
  + psw: 密碼
  + mail_address: 信箱

### 更新消費者資料
+ `/api/Customer/update_customer/`
+ Method: POST
+ Body
  + 同創建消費者
+ Return
  ```json
  {
    "status": true,
    "msg": "Update done."
  }
  ```

### 更新外送員資料
+ `/api/Delivery/update_delivery/`
+ Method: POST
+ Body
  + 同創建外送員



### 更新使用者頭像
+ `/api/Customer/update_picture/`
+ Method: POST
+ Body
  + image: image/png
  + customer_id
+ Return
  + 成功 200
    ```json
    {
      "status": true,
      "msg": "ok"
    }
    ```

### 取得使用者頭像
+ `/api/Customer/get_picture/`
+ Method: GET
+ Body
  + customer_id
+ Return
  + 成功 200 + 圖片

## 評價系統
### 評價消費者
+ `/api/Customer/do_rank/`
+ Method: POST
+ Body
  + customer_id
  + rank
+ Return
  ```json
  {
    "status": true,
    "msg": "Rank success."
  }
  ```

### 評價外送員
+ `/api/Delivery/do_rank/`
+ Method: POST
+ Body
  + delivery_id
  + rank
+ Return
  ```json
  {
    "status": true,
    "msg": "Rank success."
  }
  ```

### 取得消費者評價
+ `/api/Customer/get_rank/`
+ Method: GET
+ Body
  + customer_id
+ Return
  ```json
  {
    "status": true,
    "msg": "{rank: float}"
  }
  ```

### 評價外送員
+ `/api/Delivery/get_rank/`
+ Method: GET
+ Body
  + delivery_id
+ Return
  ```json
  {
    "status": true,
    "msg": "{rank: float}"
  }
  ```

## 登入登出
### 切換使用者模式
+ `/api/Customer/switch_mode/`
+ Method: POST
+ Body
  + customer_id
+ Return
  + 成功切換, 200
  ```json
  {
    "status": true,
    "msg": "User is switch to delivery mode",
    "mode": "delivery"
  }
  ```
  + 餐廳管理員不可切換, 406
  ```json
  {
    "status": false,
    "msg": "Restaurant manager is not able to switch mode."
  }
  ```
+ Mode
  + customer
  + delivery

### 登入
+ `/login/`
+ Method: POST
+ Body
  + JSON
    ```json
    {
        "username": "{full email address}",
        "password": "{password}"
    }
    ```
+ Return
  + 登入成功
    ```json
    {
      "status": true,
      "msg": "Login success by {user_name}"
    }
    ```
  + 密碼錯誤
    ```json
    {
      "status": false,
      "msg": "Wrong Password.",
    }
    ```
  + 無此使用者
    ```json
    {
      "status": false,
      "msg": "User not found.",
    }
    ```
  + 尚未通過驗證的使用者
    ```json
    {
      "status": false,
      "msg": "User is not verified."
    }
    ```

### 取得登入中的使用者狀態
+ `/is_login/`
+ Method: GET
+ Return
  ```json
  {
    "status": true,
    "msg": "User is login.",
    "user_id": "[customer_id]",
    "user_type": "[user_type]"
  }
  ```
+ User Type
  + customer
  + delivery
  + restaurant manager

### 登出
+ `/logout/`
+ Return
  ```json
  {
    "status": true,
    "msg": "Logout success."
  }
  ```

## 訂單
### 訂單狀態
+ ORDER_CREATE = 0 # 新建立的訂單
+ ORDER_ACCEPT = 1 # 外送中的訂單
+ ORDER_DONE_BY_USER = 2 # 使用者已確認送達
+ ORDER_DONE_BY_DELIVERY = 3 # 外送員已確認送達
+ ORDER_FINISH = 4 # 訂單已被雙方確認
+ ORDER_DELETE = 5 # 訂單已被刪除

### 創建訂單
+ `/api/Order/create_order/`
+ Method: POST
+ Body
  + customer_id: 消費者 ID
  + order_items: 訂單內容的 JSON
  + dest: 送達目的地
  + price: 訂單總價
+ Return
  + 創建成功：
    ```json
    {
        "statue": true,
        "msg": "Order create success.",
        "order_id": "{order_id}"
    }
    ```

### 接收訂單
+ `/api/Order/accept_order/`
+ Method: POST
+ Body
  + delivery_id: 外送員的 ID
  + order_id: 訂單編號
+ Return
  + 接收訂單成功：
    ```json
    {
        "statue": true,
        "msg": "Order accept success.",
        "order_id": "{order_id}"
    }
    ```

### 完成訂單
+ `/api/Order/finish_order/`
+ Method: POST
+ Body
  + order_id: 訂單編號
  + acceptor: user or delivery
+ Return
  + 確認訂單成功（其中一方）
    ```json
    {
        "statue": true,
        "msg": "Order is done by one.",
        "order_id": "{order_id}"
    }
    ```
  + 確認訂單成功（雙方皆確認）
    ```json
    {
        "statue": true,
        "msg": "Order is completely finish.",
        "order_id": "{order_id}"
    }
    ```

### 刪除訂單
+ `/api/Order/delete_order/`
+ Method: POST
+ Body
  + order_id: 訂單編號
+ Return
  + 接收訂單成功
    ```json
    {
        "statue": true,
        "msg": "Order delete success.",
        "order_id": "{order_id}"
    }
    ```

### 根據使用者 ID 查詢訂單
+ `/api/Order/query_order/`
+ Method: GET
+ Params
  + customer_id: 使用者 ID
+ Example
  + `/api/Order/query_order/?customer_id=0`
+ Return
  + 訂單的 JSON Array

### 根據外送員 ID 查詢訂單
+ `/api/Order/query_order_by_delivery/`
+ Method: GET
+ Params
  + delivery_id: 外送員 ID
+ Example
  + `/api/Order/query_order_by_delivery/?delivery_id=2`
+ Return
  + 訂單的 JSON Array

### 查詢待處理的訂單
+ `/api/Order/query_unassigned_order/`
+ Method: GET
+ Return
  + 訂單的 JSON Array

## 餐廳
### 根據餐廳管理員 ID 查詢餐廳列表
+ `/api/Restaurant/get_restaurant/`
+ Method: GET
+ Param
  + rmid: 餐廳管理員 ID
+ Return
  ```json
  {
    "restaurants": [
        {
            "restaurant_name": "[restaurant_name]",
            "phone_number": "[phone_number]",
            "address": "[address]",
            "lacation": "[經緯度]",
            "mail_address": "[mail_address]",
            "restaurant_id": "[restaurant_id]"
        },
        // ...
    ]
  }
  ```

### 更新餐廳圖片
+ `/api/Restaurant/update_picture/`
+ Method: POST
+ Body
  + rid
+ Return
  ```json
  {
    "status": true,
    "msg": "ok"
  }
  ```

### 取得餐廳圖片
+ `/api/Restaurant/get_picture/`
+ Method: GET
+ Param
  + rid
+ Return
  + 圖片本人

### 創建餐廳
+ `/api/Restaurant/create_restaurant/`
+ Method: POST
+ Body
  + address
  + lacation: 經緯度
  + mail_address
  + phone_number
  + restaurant_name
  + restaurant_owner: 餐廳管理員的 ID
+ Return
  ```json
  {
    "status": true,
    "msg": "Restaurant create success."
  }
  ```

### 更新餐廳
+ `/api/Restaurant/create_restaurant/`
+ Method: POST
+ Body
  + address
  + lacation: 經緯度
  + mail_address
  + phone_number
  + restaurant_name
  + restaurant_id: 餐廳 ID
+ Return
  ```json
  {
    "status": true,
    "msg": "Restaurant update success."
  }
  ```

### 刪除餐廳
+ `/api/Restaurant/[restaurant_id]/`
+ Method: DELETE

### 取得餐廳餐點列表
+ `/api/Restaurant/get_dishes_list/`
+ Method: GET
+ Param
  + rid: 餐廳 ID
+ Return
  ```json
  {
    "dishes": [
      {
          "item": ["咖哩沙茶", "羊肉", "蓋飯"],
          "price": 75
      },
    // ...
  ```

### 餐廳餐點更新
+ `/api/Restaurant/update_restaurant_dishes/`
+ Method: POST
+ Body
  ```json
  {
    "name": "蘋果早午餐",
    "address": "202基隆市中正區中正路816號",
    "phone": "02 2463 2928",
    "root": {
        "雷姆": 520,
        "點心": {"雷姆蛋塔": 99999},
        "烤吐司類": {
            "香烤煎蛋": 20,
            "火腿蛋": 25,
            // ...
        }, 
        // ...
  ```
+ Return
  ```json
  {
    "status": true,
    "msg": "ok"
  }
  ```

### 取得附近的餐廳
+ `/api/Restaurant/get_nearby_restaurant`
+ Method: GET
+ Param
  + x
  + y
+ Return
  ```json
    {
      "restaurant": [
        "0",
        "1",
        // ...
  ```

## 搜尋索引
### 更新索引
+ `/api/Restaurant/update_search_index/`
+ Method: GET